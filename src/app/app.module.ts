import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ParkingComponent } from './components/parking/parking.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { format } from 'date-fns';
import {HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { RouterModule } from '@angular/router';
import {ROUTES} from './app.route';
import { ReportesComponent } from './components/reportes/reportes.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CrearusuarioComponent } from './components/crearusuario/crearusuario.component';
import { Routes } from '@angular/router';
import { RentadosComponent } from './components/rentados/rentados.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';





@NgModule({
  declarations: [
    AppComponent,
    ParkingComponent,
    LoginComponent,
    ReportesComponent,
    UsuariosComponent,
    CrearusuarioComponent,
    RentadosComponent,
    NavbarComponent,
    FooterComponent,
    
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {useHash:true}),
  ],
  bootstrap: [AppComponent,
    ]
})
export class AppModule { }
