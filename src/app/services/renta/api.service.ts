import { Injectable } from '@angular/core';
import { URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';

const parkingSpots = [
  {label: '1', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0},
  {label: '2',elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
  { label: '3',elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0},
  { label: '4',elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
  { label: '5',elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0},
  { label: '6',elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
  { label: '7', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
  { label: '8', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0},
  { label: '9', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0},
  { label: '10', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0,isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
  { label: '11', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
  { label: '12', elapsedTime:0,elapsedHours:0, elapsedMinutes:0, elapsedSeconds:0,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 },
];

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  public url;
  parkingSpots = parkingSpots;

  constructor(private _http:HttpClient) {
    this.url = URL;
  }

  agregarRenta(body:any){
    const url= `${this.url}/renta/nuevo`;
    return this._http.post(url,body);
  }
  
  agregarSalida(body:any){
    const url= `${this.url}/salida/nuevo`;
    return this._http.post(url,body);
  }
  
  consultaRenta(){
    //variable propia del metodo
    const url= `${this.url}/obtener/renta`;
    return this._http.get(url);
  }
  consultaSalida(){
    //variable propia del metodo
    const url= `${this.url}/obtener/salida`;
    return this._http.get(url);
  }
  
  login(body : any) {
    const url = `${this.url}/login`;
    return this._http.post(url, body);
  }
  eliminarRenta(id:any){
    const url= `${this.url}/delete/renta/${id}`;
    return this._http.delete(url);
  }
  eliminarSalida(id:any){
    const url= `${this.url}/delete/salida/${id}`;
    return this._http.delete(url);
  }
  agregarUser(body:any){
    const url= `${this.url}/new/user`;
    return this._http.post(url,body);
  }

  agregarRentaTemporal(body:any){
    const url= `${this.url}/renta/temporal`;
    return this._http.post(url,body);
  }
  consultaTemporal(){
    //variable propia del metodo
    const url= `${this.url}/obtener/temporal`;
    return this._http.get(url);
  }
  eliminarTemporal(id:any){
    const url= `${this.url}/delete/temporal/${id}`;
    return this._http.delete(url);
  }

  eliminarSalidaTemporal(id:any){
    const url= `${this.url}/deleteSalida/temporal/${id}`;
    return this._http.delete(url);
  }
  
  actualizarTemporal(id:any, body:any){
    const url= `${this.url}/update/temporal/${id}`;
    return this._http.put(url,body);
  }
  actualizarRenta(id:any, body:any){
    const url= `${this.url}/update/renta/${id}`;
    return this._http.put(url,body);
  }
  agregarSalidaTemporal(body:any){
    const url= `${this.url}/salida/temporal`;
    return this._http.post(url,body);
  }
  consultaSalidaTemporal(){
    //variable propia del metodo
    const url= `${this.url}/obtenerSalida/temporal`;
    return this._http.get(url);
  }

  
}