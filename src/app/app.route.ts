import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Routes } from '@angular/router';
import { ParkingComponent } from './components/parking/parking.component';
import { LoginComponent } from './components/login/login.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CrearusuarioComponent } from './components/crearusuario/crearusuario.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { RentadosComponent } from './components/rentados/rentados.component';


export const ROUTES: Routes = [
  {path:'login',component: LoginComponent},
  {path:'usuarios',component: UsuariosComponent},
  {path:'CrearUsuario',component: CrearusuarioComponent},
  {path:'parking',component: ParkingComponent},
  {path:'rentados',component: RentadosComponent},
  {path:'reportes',component: ReportesComponent},

  {path: '', pathMatch:'full' ,redirectTo:'login'},
  //{path:'**', pathMatch:'full',redirectTo:'home'}
];
