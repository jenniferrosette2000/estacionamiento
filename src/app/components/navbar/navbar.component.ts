import { Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import { Router } from '@angular/router';
import { interval } from 'rxjs';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit{
  public currentDateTime: any;
  public role:any
  constructor( private _router: Router){ }
  ngOnInit(): void {
    this.currentDateTime = new Date();
    interval(1000).subscribe(() => {
      this.currentDateTime = new Date();
    });
    this.role = localStorage.getItem('role');
    console.log('role '+this.role );
  }
  
  Salir() {
    if (confirm("¿Estás seguro de que quieres hacer esto?")) {
        // El usuario hizo clic en "Aceptar"
        this._router.navigate(['login']);
    } else {
        // El usuario hizo clic en "Cancelar"
    }
  }
}
