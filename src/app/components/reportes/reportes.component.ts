import { Component, OnInit} from '@angular/core';
import { ApiService } from 'src/app/services/renta/api.service';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from 'moment';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {
  public datos2:any[]=[];
  public datosSalida:any[]=[];
  public fechaInicio=new Date();
  public fechaTermino=new Date();
  
  public reporteIngresos: any[] = [];
  public reporteSalidas: any[] = [];

  constructor(private _serviceM:ApiService){ }
  ngOnInit(): void {
    this.getRenta();
    this.getSalida();

  }

  getRenta(){
    this._serviceM.consultaRenta().subscribe((resp:any)=>{
      if (resp['ok']){
        console.log(resp);
        this.datos2=resp['respuesta'];
        console.log(this.datos2);
        
      }
    })
  }
  
  getSalida(){
    this._serviceM.consultaSalida().subscribe((resp:any)=>{
      if (resp['ok']){
        console.log(resp);
        this.datosSalida=resp['respuesta'];
        console.log(this.datosSalida);
        this.total = this.datosSalida.reduce((sum, item) => sum + item.total, 0);

      console.log(this.reporteSalidas);
      console.log("Total: " + this.total);
      }
    })
  }

  total: number = 0;
  

  generarReportes() {
    const fechaI= new Date(this.fechaInicio)
    const fechat= new Date(this.fechaTermino)
    fechaI.setDate(fechaI.getDate() + 1);
    fechat.setDate(fechat.getDate() + 1);

    console.log("fecha de inicio " + fechaI);
    console.log("fecha de termino "+fechat);
    this.reporteIngresos = this.datos2.filter(item => {
      const fecha = new Date(item.fecha);
      return fecha>=fechaI && fecha<=fechat
    });
    
    console.log(this.reporteIngresos);
    this.reporteSalidas = this.datosSalida.filter(item => {
      const fecha = new Date(item.FechaSalida)
      console.log("fecha obtenida"+fecha);
      return fecha>=fechaI && fecha<=fechat
    });
    
    console.log(this.reporteSalidas);
    this.total = this.reporteSalidas.reduce((sum, item) => sum + item.total, 0);

      console.log(this.reporteSalidas);
      console.log("Total: " + this.total);
  
    const documentDefinition = {
      content: [
        { text: 'Reporte de ingresos', style: 'header' },
        {
          table: {
            headerRows: 1,
            body: [
              ['Placa', 'Cajon', 'Fecha de ingreso', 'Hora de ingreso'],
              ...this.reporteIngresos.map(item => [item.placa, item.cajon, new Date(item.fecha).toLocaleDateString('es-ES'), item.hora])
            ]
          },
          style: 'tableStyle'
        },
        { text: 'Reporte de salidas', style: 'header' },
        {
          table: {
            headerRows: 1,
            body: [
              ['Placa', 'Cajon', 'Fecha de salida', 'Hora de salida', 'Tiempo', 'Total'],
              ...this.reporteSalidas.map(item => [item.placa, item.cajon, new Date(item.FechaSalida).toLocaleDateString('es-ES') , item.horaSalida, item.tiempo, item.total]),
              ['', '', '', '', 'Total', this.total.toFixed(2)]

            ]
          }
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10] as [number, number, number, number]
        },
        tableStyle: {
          fontSize: 12 // Tamaño de fuente opcional
        }
      }
      
    };
    // Generar el documento PDF y abrirlo en una nueva ventana del navegador
    pdfMake.createPdf(documentDefinition).open();
    this.reporteIngresos.length=0;
  }
    
  
}
  



