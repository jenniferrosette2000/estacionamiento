import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentadosComponent } from './rentados.component';

describe('RentadosComponent', () => {
  let component: RentadosComponent;
  let fixture: ComponentFixture<RentadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentadosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RentadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
