import { Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import { ApiService } from 'src/app/services/renta/api.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from 'moment';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-rentados',
  templateUrl: './rentados.component.html',
  styleUrls: ['./rentados.component.css']
})
export class RentadosComponent implements OnInit {
  
  public datos2:any[]=[];//CONSULTA TEMPORAL
  public datos3:any[]=[];//SALIDA TEMPORAL

  @ViewChild("myModalInfo", {static: false}) myModalInfo!: TemplateRef<any>; // inicialización con valor nulo
  datos={id:'',placa:'', nombreD:'', cantidad:0, totalT:0, periodo:''}

  public datosSalida:any[]=[];// X
  public fechaInicio=new Date(); // "FECHA DE INICIO" PARA LOS REPORTES
  public fechaTermino=new Date();// "FECHA DE SALIDA" PARA LOS REPORTES
  
  public reporteIngresos: any[] = []; // GENERAR REPORTES "INGRESOS"
  public reporteSalidas: any[] = [];  // GENERAR REPORTES "SALIDAS"

  ngOnInit(): void {
    this.getTemporal()
    this.getSalidaTemporal()
  }

  constructor(private _serviceM:ApiService, private modalService: NgbModal){ }

  mostrarModalInfo(spot:any){
    this.modalService.open(this.myModalInfo);
    this.datos.id=spot._id;
    this.datos.placa=spot.placa;
    this.datos.nombreD=spot.nombreD;
    this.datos.cantidad=spot.cantidad;
    this.datos.periodo=spot.periodo;
  }

  getTemporal(){
    this._serviceM.consultaTemporal().subscribe((resp:any)=>{
      if (resp['ok']){
        //console.log(resp);
        this.datos2=resp['respuesta'];
        //console.log(this.datos2);
        this.total = this.datos2.reduce((sum, item) => sum + item.totalT, 0);
      }
    })
  }

  getSalidaTemporal(){
    this._serviceM.consultaSalidaTemporal().subscribe((resp:any)=>{
      if (resp['ok']){
        //console.log(resp);
        this.datos3=resp['respuesta'];
        //console.log(this.datos3);
      }
    })
  }



  EliminarTemporal(id: string){    
    this._serviceM.eliminarTemporal(id).subscribe((data:any)=>{
      //console.log(id);
      this.getTemporal();
    }) 
  }

  EliminarSalidaTemporal(id: string){    
    this._serviceM.eliminarSalidaTemporal(id).subscribe((data:any)=>{
      //console.log(id);
      this.getSalidaTemporal();
    }) 
  }

  mostrarConfirmacionEliminarSalida(spot:any): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.EliminarSalidaTemporal(spot);
      }
      return result.isConfirmed;
    });
  }

  mostrarConfirmacion(spot:any): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.EliminarTemporal(spot);
      }
      return result.isConfirmed;
    });
  }

  mostrarConfirmacionUpdate(): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'Dar clic en si para actualizar registro',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Actualizar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.Actualizar();
      }
      return result.isConfirmed;
    });
  }

  Actualizar(){   
    if(this.datos.placa=='' || this.datos.nombreD=='' || this.datos.cantidad==0){
      Swal.fire({
        icon: 'warning',
        title: 'Rellena los campos vacios',
        confirmButtonText: 'Aceptar',
      });
    }
    else{
      if (this.datos.periodo=="Dia"){
        this.datos.totalT=100*this.datos.cantidad
      }
      if (this.datos.periodo=="Semana"){
        this.datos.totalT=600*this.datos.cantidad
      }
      if (this.datos.periodo=="Mes"){
        this.datos.totalT=2000*this.datos.cantidad
      }
      this._serviceM.actualizarTemporal(this.datos.id, this.datos).subscribe((data:any)=>{
        //console.log(this.datos);
        this.getTemporal()
        this.datos={id:'',placa:'', nombreD:'', cantidad:0, totalT:0, periodo:''}
      })
    }
  }

  total: number = 0;
  

  generarReportes() {
    const fechaI= new Date(this.fechaInicio)
    const fechat= new Date(this.fechaTermino)
    fechaI.setDate(fechaI.getDate() + 1);
    fechat.setDate(fechat.getDate() + 1);

    //console.log("fecha de inicio " + fechaI);
    //console.log("fecha de termino "+fechat);
    this.reporteIngresos = this.datos2.filter(item => {
      const fecha = new Date(item.fecha);
      return fecha>=fechaI && fecha<=fechat
    });
    
    //console.log(this.reporteIngresos);
   
    
    this.total = this.reporteIngresos.reduce((sum, item) => sum + item.totalT, 0);
    //console.log(this.reporteSalidas);
    console.log("Total: " + this.total);
  
    const documentDefinition = {
      content: [
        { text: 'Reporte de ingresos', style: 'header' },
        {
          table: {
            headerRows: 1,
            body: [
              ['Placa', 'Nombre Dueño', 'Cajon', 'Periodo', 'Cantidad', 'Fecha', 'Total'],
              ...this.reporteIngresos.map(item => [item.placa, item.nombreD, item.cajon, item.periodo, item.cantidad, new Date(item.fecha).toLocaleDateString('es-ES'), item.totalT]),
              ['','', '', '', '', 'Total', this.total.toFixed(2)]

            ]
          }
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10] as [number, number, number, number]
        }
      }
      
    };
    // Generar el documento PDF y abrirlo en una nueva ventana del navegador
    pdfMake.createPdf(documentDefinition).open();
    this.reporteIngresos.length=0;
  }
}
