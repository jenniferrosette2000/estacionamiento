import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/renta/api.service';
import { Router } from '@angular/router';
import { data } from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  datosLogin = {email: '', password: ''};

  constructor(private _service:ApiService, private _router: Router) { }

  ngOnInit(): void {
  }

  login() {
    if (this.datosLogin.email == '' || this.datosLogin.password == '') {
      alert('Campos vacíos');

    }else {
    console.log(this.datosLogin);
    this._service.login(this.datosLogin).subscribe((resp:any) => {

      if (resp.role == 'Administrador'){
        localStorage.setItem('role', 'Administrador');

      this._router.navigate(['parking']);
      }
      if (resp.role == 'Empleado'){
      localStorage.setItem('role', 'Empleado');
      this._router.navigate(['parking']);
      }

      if (resp.mensaje == 'contraseña mal'){
        alert('Password Incorrecta');
      }

      if (resp.mensaje == 'email no registrado'){
        alert('No existe el email');
      }
    })
  }
  }

}
