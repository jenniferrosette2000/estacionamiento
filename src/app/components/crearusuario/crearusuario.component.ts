import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/renta/api.service';

@Component({
  selector: 'app-crearusuario',
  templateUrl: './crearusuario.component.html',
  styleUrls: ['./crearusuario.component.css']
})
export class CrearusuarioComponent implements OnInit {
  hide = true;
  Datos={nombreUsuario:'', email:'', password:'', role:''}//OBJETO QUE GUARDARA LOS DATOS DEL USUARIO

  constructor( private _serviceM:ApiService) { }

  ngOnInit(): void {
  }

  AgregarUser(){
    if(this.Datos.nombreUsuario=='' || this.Datos.email=='' || this.Datos.password=='' || this.Datos.role==''){
      //COMPRUEBA SI EXISTEN CAMPOS VACIOS
      window.alert("Debes ingresar todos los datos solicitados")
    }
    else{
      console.log(this.Datos);//DATOS REGISTRADOS MOSTRADOS EN CONSOLA
    this._serviceM.agregarUser(this.Datos).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
      if (data ['ok']) {//SI LOS DATOS ESTAN COMPLETOS LO REGISTRA
        alert("Registrado exitosamente");
      } else
      alert(data.error);//EN CASO CONTRARIO MANDA UN ERROR
    })}
  }
}
