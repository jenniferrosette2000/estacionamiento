import { Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { format } from 'date-fns';
import { ApiService } from 'src/app/services/renta/api.service';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from 'moment';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
import { Router } from '@angular/router';
import { interval } from 'rxjs';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';



@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.css']
})


export class ParkingComponent implements OnInit{

  @ViewChild("myModalInfo", {static: false}) myModalInfo!: TemplateRef<any>; 
  @ViewChild("myModalInfo2", {static: false}) myModalInfo2!: TemplateRef<any>; 
  @ViewChild("myModalConf", {static: false}) myModalConf!: TemplateRef<any>; 
  @ViewChild("myModalConf2", {static: false}) myModalConf2!: TemplateRef<any>; 
  @ViewChild("myModalInfo3", {static: false}) myModalInfo3!: TemplateRef<any>; 

  public currentDateTime: any;

  public fechaActual:any;
  public fechaActualSalida:any;

  public currentTime: any;
  public elapsedTime: any;
  public elapsedHours: any;
  public elapsedMinutes: any;
  public elapsedSeconds: any;

  public total2=0;
  public placa: any;
  public cajon: any;

  public reporteIngresos: any[] = [];
  public reporteSalidas: any[] = [];

  Datos={placa:'', cajon:'', fecha:'', hora:''}
  Datos2={placa:'',cajon:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0}
  public DatosT={placa:'',nombreD:'', cajon:'', periodo:'', cantidad:0, fechaIngreso:'', totalT:0}
  public datosU1={id:'', placa:''}


  public datos2:any[]=[];
  public datosSalida:any[]=[];
  public datosS:any[]=[];
  public localStorageService:any;
  public datosSpot:any[]=[];
  public role:any
  isDisabled = true;
  
  constructor(private modalService: NgbModal,  private _serviceM:ApiService, private _router: Router){ 

  }
  
  ngOnInit(): void {
    this.getRenta();
    this.getSalida();
    this.initializeParkingSpots()
    this.currentDateTime = new Date();
    interval(1000).subscribe(() => {
      this.currentDateTime = new Date();
    });
    this.role = localStorage.getItem('role');
    console.log('role '+this.role );
    

  }

  
initializeParkingSpots() {
  const parkingSpots = localStorage.getItem('parkingSpots');
  if (parkingSpots === null) {
    // Si el arreglo no existe en el LocalStorage, se inicializa con los valores por defecto
    this.parkingSpots = [
      {label: '1', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0, totalT:0 },
      {label: '2', isSelectedR: true,isSelectedE: true, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 , nombreD:'', periodo:'', cantidad:0 , totalT:0 },
      { label: '3', isSelectedR: true, isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '4', isSelectedR: true, isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0  ,totalT:0 },
      { label: '5', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '6', isSelectedR: true,isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 , nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '7', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 , nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '8', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '9',isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '10',isSelectedR: true,isSelectedE: true,  sOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 ,nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
      { label: '11',isSelectedR: true,isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0  },
      { label: '12', isSelectedR: true,isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0  ,totalT:0 },
    
    ];
    this.saveParkingSpotsToLocalStorage();
  } else {
    // Si el arreglo existe en el LocalStorage, se carga desde allí
    this.parkingSpots = JSON.parse(parkingSpots);
  }
}

validateKeyPress(event: KeyboardEvent) {
  const inputChar = String.fromCharCode(event.keyCode || event.which);
  const isNumber = /^[0-9]+$/.test(inputChar);
  const isMinusSign = inputChar === '-';
  const isExponentialChar = inputChar.toLowerCase() === 'e';

  if (!isNumber || isMinusSign || isExponentialChar) {
    event.preventDefault();
  }
}

saveParkingSpotsToLocalStorage() {
  //convierte el arreglo de espacios de estacionamiento en formato JSON y lo guarda en el LocalStorage bajo la clave 'parkingSpots'
  localStorage.setItem('parkingSpots', JSON.stringify(this.parkingSpots));
}

  getRenta(){
    this._serviceM.consultaRenta().subscribe((resp:any)=>{
      if (resp['ok']){
        //console.log(resp);
        this.datos2=resp['respuesta'];
        this.datos2.reverse(); // Invierte el orden del arreglo

        //console.log(this.datos2);
      }
    })
  }
  
  getSalida(){
    this._serviceM.consultaSalida().subscribe((resp:any)=>{
      if (resp['ok']){
        //console.log(resp);
        this.datosSalida=resp['respuesta'];
        this.datosSalida.reverse(); // Invierte el orden del arreglo

        //console.log(this.datosSalida);
      }
    })
  }

  mostrarModalInfo(spot:any){
    this.placa='';
    this.fechaActual = new Date();
    this.fechaActualSalida = new Date();
    this.modalService.open(this.myModalInfo);
    this.cajon = spot.label;
  }

  mostrarModalInfo2(spot:any){
    this.fechaActual = new Date();
    this.modalService.open(this.myModalInfo2);
    this.cajon = spot.label;
  }

  mostrarModalConf(spot:any){
    this.cajon = spot.label;
    this.modalService.open(this.myModalConf).result.then( r => {
      this.fechaActualSalida = new Date();
    }, error => {
      console.log(error);

    });
  }
  

  mostrarModalConf2(spot:any){
    this.fechaActualSalida = new Date();
    this.cajon = spot.label;
    this.modalService.open(this.myModalConf2).result.then( r => {
    }, error => {
      console.log(error);

    });
  }

  mostrarModalUptade1(spot:any){
    this.modalService.open(this.myModalInfo3);
    this.datosU1.id=spot._id;
    this.datosU1.placa=spot.placa
  }

  parkingSpots = [
    {label: '1', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0, totalT:0 },
    {label: '2', isSelectedR: true,isSelectedE: true, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 , nombreD:'', periodo:'', cantidad:0 , totalT:0 },
    { label: '3', isSelectedR: true, isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '4', isSelectedR: true, isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0  ,totalT:0 },
    { label: '5', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '6', isSelectedR: true,isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 , nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '7', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 , nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '8', isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '9',isSelectedR: true, isSelectedE: true, isOccupied: false, intervalId: null, time: '',  placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '10',isSelectedR: true,isSelectedE: true,  sOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0 ,nombreD:'', periodo:'', cantidad:0 ,totalT:0 },
    { label: '11',isSelectedR: true,isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0 ,totalT:0  },
    { label: '12', isSelectedR: true,isSelectedE: true,  isOccupied: false, intervalId: null, time: '', placa:'',cajon:'',fecha:'', hora:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0, nombreD:'', periodo:'', cantidad:0  ,totalT:0 },
  
  ];

  selectedSpot: any = null;


  toggleSpot(spot:any) {
    //cambia el estado de ocupación de un espacio de estacionamiento y actualiza los datos correspondientes
    const spotIndex = this.parkingSpots.indexOf(spot);
    //console.log(spotIndex);
    if (spot.isOccupied) {
      clearInterval(spot.intervalId);
      spot.intervalId = null;
      spot.time = '';
      this.parkingSpots[spotIndex].isOccupied = false;
    }
     else {
      this.selectedSpot = spot; // Aquí se asigna el objeto spot seleccionado
      this.parkingSpots[spotIndex].isOccupied = true;
      
    }
    this.saveParkingSpotsToLocalStorage();
  }

  
  guardarTemporal(){
    if(this.DatosT.placa=='' || this.DatosT.nombreD=='' || this.DatosT.periodo=='' || this.DatosT.cantidad==0){
      Swal.fire({
        icon: 'warning',
        title: 'Datos vacios',
        text: 'Ingresa los datos faltantes',
      })
    } 
    else{
      this.fechaActual = new Date();
      const fecha = format(this.fechaActual, 'dd/MM/yyyy');
      const hora = format(this.fechaActual, 'HH:mm:ss');
      this.Datos.placa=this.placa;
      this.Datos.cajon=this.cajon;
      this.Datos.fecha=fecha;
      this.Datos.hora=hora;
      for (let index = 0; index < this.parkingSpots.length; index++) {
        if(this.cajon==this.parkingSpots[index].label){
          if(this.parkingSpots[index].isOccupied){
            Swal.fire({
              icon: 'warning',
              title: 'El cajon se encuentra ocupado',
              text: 'No se pudo realizar la acción',
            })
          }
          else{
          this.fechaActual = new Date();
          this.parkingSpots[index].placa=this.DatosT.placa;
          this.parkingSpots[index].nombreD=this.DatosT.nombreD;
          this.parkingSpots[index].cajon=this.cajon;
          this.parkingSpots[index].periodo=this.DatosT.periodo;
          this.parkingSpots[index].cantidad=this.DatosT.cantidad;
          this.parkingSpots[index].fecha=this.fechaActual;
          if (this.parkingSpots[index].periodo=="Dia"){
            this.DatosT.totalT=100*this.DatosT.cantidad;
            this.parkingSpots[index].totalT=this.DatosT.totalT;
          }
          if (this.parkingSpots[index].periodo=="Semana"){
            this.DatosT.totalT=600*this.DatosT.cantidad;
            this.parkingSpots[index].totalT=this.DatosT.totalT;
          }
          if (this.parkingSpots[index].periodo=="Mes"){
            this.DatosT.totalT=2000*this.DatosT.cantidad;
            this.parkingSpots[index].totalT=this.DatosT.totalT;
          }
          this.parkingSpots[index].isSelectedR = true;
          this.parkingSpots[index].isSelectedE= false;
          this._serviceM.agregarRentaTemporal(this.parkingSpots[index]).subscribe((data:any)=>{
            if(data.ok){
              //alert("Registro agregado correctamente");
              //console.log(this.parkingSpots[index]);
              this.toggleSpot(this.parkingSpots[index])
              Swal.fire({
                icon: 'success',
                title: 'Inserción exitosa',
                text: 'Se insertó correctamente el registro',
              }).then((result) => {
                if (result.isConfirmed) {
                  //this.seleccionarSpot(this.parkingSpots[index]);
                  window.location.reload();
                }
              });
            }
          }) 
        }
    } 
  }
    
    this.saveParkingSpotsToLocalStorage();
    }
  }



  guardar(){
    if(this.placa=='' || this.cajon==''){
      Swal.fire({
        icon: 'warning',
        title: 'Datos vacios',
        text: 'Ingresa los datos faltantes',
      })
    } 
    else{
      
      this.fechaActual = new Date();
      const fecha = format(this.fechaActual, 'dd/MM/yyyy');
      const hora = format(this.fechaActual, 'HH:mm:ss');
      for (let index = 0; index < this.parkingSpots.length; index++) {
        if(this.cajon==this.parkingSpots[index].label){
          if(this.parkingSpots[index].isOccupied){
            Swal.fire({
              icon: 'warning',
              title: 'El cajon se encuentra ocupado',
              text: 'No se pudo realizar la acción',
            })
          }
          else{
          this.fechaActual = new Date();
          const fecha = format(this.fechaActual, 'dd/MM/yyyy');
          const hora = format(this.fechaActual, 'HH:mm:ss');
          this.parkingSpots[index].placa=this.placa;
          this.parkingSpots[index].cajon=this.cajon;
          this.parkingSpots[index].fecha=this.fechaActual;
          this.parkingSpots[index].hora=hora;
          this.parkingSpots[index].isSelectedE = true;
          this.parkingSpots[index].isSelectedR = false;
          this._serviceM.agregarRenta(this.parkingSpots[index]).subscribe((data:any)=>{
            if(data.ok){
              //alert("Registro agregado correctamente");
              //console.log(this.Datos);
              this.toggleSpot(this.parkingSpots[index])

              this.Datos={placa:'', cajon:'', fecha:'', hora:''}
              this.getRenta();
              Swal.fire({
                icon: 'success',
                title: 'Inserción exitosa',
                text: 'Se insertó correctamente el registro',
              }).then((result) => {
                if (result.isConfirmed) {
                  window.location.reload();
                  //this.seleccionarSpot(this.parkingSpots[index])
                }
              });
            }
          }) 
        }
      }
    } 
  }
    this.saveParkingSpotsToLocalStorage();
  }
  
  VerificarSalida(){
    this.fechaActualSalida = new Date();
    const FechaSalida2 = format(this.fechaActualSalida, 'dd/MM/yyyy');
    const horaSalida2 = format(this.fechaActualSalida, 'HH:mm:ss');
    for (let index = 0; index < this.parkingSpots.length; index++) {
      if(this.cajon==this.parkingSpots[index].label){
        if(!this.parkingSpots[index].isOccupied){
          Swal.fire({
            icon: 'warning',
            title: 'El cajon no se encuentra ocupado',
            text: 'No se pudo realizar la acción',
          })
        }
        else{
        const fecha1 = new Date();
        const fecha2 = new Date(this.parkingSpots[index].fecha);
        const diferenciaMs = fecha1.getTime() - fecha2.getTime();
        // Calcular las horas, minutos y segundos a partir de la diferencia de tiempo
        const segundos = Math.floor(diferenciaMs / 1000) % 60;
        const minutos = Math.floor(diferenciaMs / 1000 / 60) % 60;
        const horas = Math.floor(diferenciaMs / 1000 / 60 / 60);
        this.parkingSpots[index].tiempo= `${horas}:${minutos}:${segundos}`;
        this.Datos2.placa=this.parkingSpots[index].placa;
        this.Datos2.cajon=this.parkingSpots[index].cajon;
        this.Datos2.FechaSalida=this.fechaActualSalida;
          this.Datos2.horaSalida=horaSalida2;
          this.Datos2.tiempo=this.parkingSpots[index].tiempo;
          const tarifaPorHora = 10;
          // Si el coche ha estado en el estacionamiento durante menos de una hora completa, cobramos la tarifa de una hora completa
          let cobroTotal = 0;
          if (horas > 1) {
          // Si el coche ha estado en el estacionamiento por más de una hora, redondeamos hacia arriba y cobramos por cada hora adicional completa
          cobroTotal += (horas - 1) * tarifaPorHora;
          }
          // Si hay tiempo adicional en minutos o segundos, redondeamos hacia arriba y cobramos por una hora adicional completa
          if (minutos > 0 || segundos > 0) {
          cobroTotal += tarifaPorHora;
          }
          this.parkingSpots[index].total=cobroTotal
          this.Datos2.total=cobroTotal
          //console.log(this.Datos2);
          this.parkingSpots[index].isSelectedR = true;
          this.parkingSpots[index].isSelectedE = true;
          this._serviceM.agregarSalida(this.Datos2).subscribe((data:any)=>{
            if(data.ok){
              alert("El vehículo ha salido");
              this.toggleSpot(this.parkingSpots[index])
              this.getSalida();
              this.Datos2={placa:'',cajon:'', FechaSalida:'', horaSalida:'', tiempo:'', total:0}
              window.location.reload();


            }
          }) 
      }
    } 
    }
    this.saveParkingSpotsToLocalStorage();
  }


  agregarSalidaTemporal(){
    for (let index = 0; index < this.parkingSpots.length; index++) {
    if(this.cajon==this.parkingSpots[index].label){
      if(this.parkingSpots[index].isOccupied==false){
        Swal.fire({
          icon: 'warning',
          title: 'El cajon no se encuentra ocupado',
          text: 'No se pudo realizar la acción',
        })
      }
      else{
      //console.log(this.parkingSpots[index]);
      this.DatosT.periodo=this.parkingSpots[index].periodo
      this.DatosT.totalT=this.parkingSpots[index].totalT
      this.parkingSpots[index].isSelectedE = true;
      this.parkingSpots[index].isSelectedR = true;
      this._serviceM.agregarSalidaTemporal(this.parkingSpots[index]).subscribe((data:any)=>{
        if(data.ok){
          alert("El vehículo ha salido");
          this.toggleSpot(this.parkingSpots[index])
          this.Datos={placa:'', cajon:'', fecha:'', hora:''}
          this.getRenta();
          window.location.reload();
      }
    }) 
  }
}

  
}
this.saveParkingSpotsToLocalStorage();
}
  mostrarConfirmacionEliminar1(spot:any): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.EliminarRenta(spot);
      }
      return result.isConfirmed;
    });
  }

  mostrarConfirmacionEliminar2(spot:any): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.EliminarSalida(spot);
      }
      return result.isConfirmed;
    });
  }

  EliminarRenta(id: string){    
    this._serviceM.eliminarRenta(id).subscribe((data:any)=>{
      //console.log(id);
      this.getRenta();
    }) 
  }
  EliminarSalida(id: string){    
    this._serviceM.eliminarSalida(id).subscribe((data:any)=>{
      //console.log(id);
      this.getSalida();
    }) 
  }


  mostrarConfirmacionUpdate(): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'Dar clic en si para actualizar registro',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Actualizar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.actualizar();
      }
      return result.isConfirmed;
    });
  }

  
  actualizar(){
    this._serviceM.actualizarRenta(this.datosU1.id, this.datosU1).subscribe((data:any)=>{
      //console.log(this.datosU1);
      this.getRenta()
      this.datosU1={id:'',placa:''}
    })
    }
  
  public fechaInicio=new Date();
  public fechaTermino=new Date();

  generarReportes() {
    const fechaI= new Date(this.fechaInicio)
    const fechat= new Date(this.fechaTermino)

    //console.log("fecha de inicio " + this.fechaInicio);
    //console.log("fecha de termino "+this.fechaTermino);
    this.reporteIngresos = this.datos2.filter(item => {
      const fecha = new Date(item.fecha);
      var anio = fecha.getUTCFullYear();
      var mes = fecha.getUTCMonth() + 1; // Los meses empiezan en cero, por lo que se debe agregar 1
      var dia = fecha.getUTCDate();
      const fechaComparar1= dia + "/" + mes + "/" + anio
      const fechaComparar= new Date (fechaComparar1);
      return fechaComparar>=fechaI && fechaComparar<=fechat
    });

    //console.log(this.reporteIngresos);
    this.reporteSalidas = this.datosSalida.filter(item => {
      const fecha = new Date(item.FechaSalida);
      var anio = fecha.getUTCFullYear();
      var mes = fecha.getUTCMonth() + 1; // Los meses empiezan en cero, por lo que se debe agregar 1
      var dia = fecha.getUTCDate();
      const fechaComparar1= dia + "/" + mes + "/" + anio
      const fechaComparar= new Date (fechaComparar1);
      return fechaComparar>=fechaI && fechaComparar<=fechat
    });
    
    //console.log(this.reporteSalidas);

    const documentDefinition = {
      content: [
        { text: 'Reporte de ingresos', style: 'header' },
        {
          table: {
            headerRows: 1,
            body: [
              ['Placa', 'Cajon', 'Fecha de ingreso', 'Hora de ingreso'],
              ...this.reporteIngresos.map(item => [item.placa, item.cajon, item.fecha, item.hora])
            ]
          }
        },
        { text: 'Reporte de salidas', style: 'header' },
        {
          table: {
            headerRows: 1,
            body: [
              ['Placa', 'Cajon', 'Fecha de salida', 'Hora de salida', 'Tiempo', 'Total'],
              ...this.reporteSalidas.map(item => [item.placa, item.cajon, item.FechaSalida, item.horaSalida, item.tiempo, item.total])
            ]
          }
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10] as [number, number, number, number]
        }
      }
      
    };
    // Generar el documento PDF y abrirlo en una nueva ventana del navegador
    pdfMake.createPdf(documentDefinition).open();
    this.reporteIngresos.length=0;
  }
    
   Salir() {
    if (confirm("¿Estás seguro de que quieres hacer esto?")) {
        // El usuario hizo clic en "Aceptar"
        this._router.navigate(['login']);
    } else {
        // El usuario hizo clic en "Cancelar"
    }
}
}